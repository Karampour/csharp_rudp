// <auto-generated>
//  automatically generated by the FlatBuffers compiler, do not modify
// </auto-generated>

namespace Packets
{

using global::System;
using global::System.Collections.Generic;
using global::FlatBuffers;

public struct Ping : IFlatbufferObject
{
  private Table __p;
  public ByteBuffer ByteBuffer { get { return __p.bb; } }
  public static void ValidateVersion() { FlatBufferConstants.FLATBUFFERS_2_0_0(); }
  public static Ping GetRootAsPing(ByteBuffer _bb) { return GetRootAsPing(_bb, new Ping()); }
  public static Ping GetRootAsPing(ByteBuffer _bb, Ping obj) { return (obj.__assign(_bb.GetInt(_bb.Position) + _bb.Position, _bb)); }
  public void __init(int _i, ByteBuffer _bb) { __p = new Table(_i, _bb); }
  public Ping __assign(int _i, ByteBuffer _bb) { __init(_i, _bb); return this; }

  public long TimeStamp { get { int o = __p.__offset(4); return o != 0 ? __p.bb.GetLong(o + __p.bb_pos) : (long)0; } }

  public static Offset<Packets.Ping> CreatePing(FlatBufferBuilder builder,
      long time_stamp = 0) {
    builder.StartTable(1);
    Ping.AddTimeStamp(builder, time_stamp);
    return Ping.EndPing(builder);
  }

  public static void StartPing(FlatBufferBuilder builder) { builder.StartTable(1); }
  public static void AddTimeStamp(FlatBufferBuilder builder, long timeStamp) { builder.AddLong(0, timeStamp, 0); }
  public static Offset<Packets.Ping> EndPing(FlatBufferBuilder builder) {
    int o = builder.EndTable();
    return new Offset<Packets.Ping>(o);
  }
  public PingT UnPack() {
    var _o = new PingT();
    this.UnPackTo(_o);
    return _o;
  }
  public void UnPackTo(PingT _o) {
    _o.TimeStamp = this.TimeStamp;
  }
  public static Offset<Packets.Ping> Pack(FlatBufferBuilder builder, PingT _o) {
    if (_o == null) return default(Offset<Packets.Ping>);
    return CreatePing(
      builder,
      _o.TimeStamp);
  }
}

public class PingT
{
  public long TimeStamp { get; set; }

  public PingT() {
    this.TimeStamp = 0;
  }
}


}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FlatBuffers;
using Packets;
using UnityEngine;
using Random = UnityEngine.Random;

public class RUDP : MonoBehaviour
{
    public string ServerAddress;
    public int Port;
    public int DropChance;
    public long Ping;

    private readonly CancellationTokenSource cts = new CancellationTokenSource();
    private readonly Queue<InternalPacketT> m_BackLog = new Queue<InternalPacketT>();
    private readonly Dictionary<ulong, toBeAcknowledged> m_ToBeACK = new Dictionary<ulong, toBeAcknowledged>();
    private UdpClient m_Client;
    private ulong m_LastACKpacket;
    private ulong m_LastRecvPacket;
    private ulong m_LastRequiredSeqNum;
    private ulong m_NextSequenceNum = 1;
    public Queue<byte[]> RecvQ = new Queue<byte[]>();

    private int Tick;

    private void Awake()
    {
        m_Client = new UdpClient();
        // m_Client.DontFragment = true;
    }

    // Start is called before the first frame update
    private void Start()
    {
        ConnectToServer(ServerAddress, Port);
        ReadAsync(DropChance);
        CheckAckAsync();
        //SendData(Encoding.UTF8.GetBytes("alireza"), true);
    }

    // Update is called once per frame
    private void Update()
    {
        while (RecvQ.Count > 0)
        {
            Tick t = Packets.Tick.GetRootAsTick(new ByteBuffer(RecvQ.Dequeue()));
            TickT tick = t.UnPack();
            print($"tick:{tick.Tick_}");
        }
    }

    private void OnDestroy()
    {
        cts.Cancel();
    }

    public void ConnectToServer(string ip, int port)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            m_Client.Connect(IPAddress.Parse(ip), port);
            print($"{IPAddress.Parse(ip)}:{port}");


            SYNT syn = new SYNT
            {
                LocalAddr = "",
                RemoteAddr = ""
            };

            FlatBufferBuilder b = new FlatBufferBuilder(1024);
            b.Finish(SYN.Pack(b, syn).Value);


            InternalPacketT internalPacket = new InternalPacketT
            {
                Type = "SYN",
                ShouldRecvAck = true,
                SequenceNumber = m_NextSequenceNum,
                LastRequiredSeqNum = m_LastRequiredSeqNum,
                Payload = new List<byte>(b.SizedByteArray())
            };

            if (internalPacket.SequenceNumber > m_LastRequiredSeqNum && internalPacket.ShouldRecvAck)
                m_LastRequiredSeqNum = internalPacket.SequenceNumber;
            m_NextSequenceNum++;

            b = new FlatBufferBuilder(1);
            b.Finish(InternalPacket.Pack(b, internalPacket).Value);


            m_Client.Send(b.SizedByteArray(), b.SizedByteArray().Length);


            if (internalPacket.ShouldRecvAck)
            {
                toBeAcknowledged toBeAcknowledged = new toBeAcknowledged
                {
                    DeadLine = Time.time + Ping,
                    InternalPacket = internalPacket
                };
                m_ToBeACK.Add(toBeAcknowledged.InternalPacket.SequenceNumber, toBeAcknowledged);
            }
        }
    }

    public void SendData(byte[] data, bool isACKrequierd)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            InternalPacketT internalPacket = new InternalPacketT
            {
                ShouldRecvAck = isACKrequierd,
                SequenceNumber = m_NextSequenceNum,
                LastRequiredSeqNum = m_LastRequiredSeqNum,
                Payload = new List<byte>(data)
            };
            m_NextSequenceNum++;

            if (internalPacket.SequenceNumber > m_LastRequiredSeqNum && isACKrequierd)
                m_LastRequiredSeqNum = internalPacket.SequenceNumber;

            if (isACKrequierd)
            {
                toBeAcknowledged toBeAcknowledged = new toBeAcknowledged
                {
                    DeadLine = Time.time + Ping,
                    InternalPacket = internalPacket
                };
                m_ToBeACK.Add(toBeAcknowledged.InternalPacket.SequenceNumber, toBeAcknowledged);
            }

            FlatBufferBuilder b = new FlatBufferBuilder(1);
            b.Finish(InternalPacket.Pack(b, internalPacket).Value);

            m_Client.Send(b.SizedByteArray(), b.SizedByteArray().Length);
        }
    }

    public void SendData(byte[] data, bool isACKrequierd, string type)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            InternalPacketT internalPacket = new InternalPacketT
            {
                Type = type,
                ShouldRecvAck = isACKrequierd,
                SequenceNumber = m_NextSequenceNum,
                LastRequiredSeqNum = m_LastRequiredSeqNum,
                Payload = new List<byte>(data)
            };
            m_NextSequenceNum++;

            if (internalPacket.SequenceNumber > m_LastRequiredSeqNum && isACKrequierd)
                m_LastRequiredSeqNum = internalPacket.SequenceNumber;

            if (isACKrequierd)
            {
                toBeAcknowledged toBeAcknowledged = new toBeAcknowledged
                {
                    DeadLine = Time.time + Ping,
                    InternalPacket = internalPacket
                };
                m_ToBeACK.Add(toBeAcknowledged.InternalPacket.SequenceNumber, toBeAcknowledged);
            }

            FlatBufferBuilder b = new FlatBufferBuilder(1);
            b.Finish(InternalPacket.Pack(b, internalPacket).Value);

            m_Client.Send(b.SizedByteArray(), b.SizedByteArray().Length);
        }
    }

    public async void ReadAsync(int dropChance)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            while (!cts.Token.IsCancellationRequested)
            {
                ms.Position = 0;
                ms.SetLength(0);

                int len = m_BackLog.Count;
                for (int i = 0; i < len; i++)
                {
                    InternalPacketT p = m_BackLog.Dequeue();
                    EvaluatePacket(p);
                }

                UdpReceiveResult data = await m_Client.ReceiveAsync();
                //packet lost simulation

                //print(Encoding.UTF8.GetString(data.Buffer));
                InternalPacket IP = InternalPacket.GetRootAsInternalPacket(new ByteBuffer(data.Buffer));
                InternalPacketT internalPacket = IP.UnPack();


                int rnd = Random.Range(0, 101);
                if (rnd < dropChance)
                {
                    print($"Droped!:{Encoding.UTF8.GetString(data.Buffer)} ");
                    continue;
                }

                //print(Encoding.UTF8.GetString(data.Buffer));


                if (internalPacket.Type == "ACK")
                {
                    ACK a = ACK.GetRootAsACK(new ByteBuffer(internalPacket.Payload.ToArray()));
                    // print(Encoding.UTF8.GetString(json));
                    ACKT ack = a.UnPack();
                    ms.SetLength(0);
                    if (m_ToBeACK.ContainsKey(ack.SequenceNumber))
                        m_ToBeACK.Remove(ack.SequenceNumber);
                    print($"ACK in -> {ack.SequenceNumber}");

                    continue;
                }

                if (internalPacket.Type == "SYN")
                {
                    SYN s = SYN.GetRootAsSYN(new ByteBuffer(internalPacket.Payload.ToArray()));

                    SYNT syn = s.UnPack();
                    ms.SetLength(0);

                    print($"SYN in -> local: {syn.LocalAddr} - remote: {syn.RemoteAddr} ");

                    string[] addr = syn.RemoteAddr.Split(':');
                    if (addr.Length != 2) continue;

                    string host = addr[0];
                    string port = addr[1];


                    ServerAddress = host;
                    Port = int.Parse(port);

                    ACKT ack = new ACKT { SequenceNumber = internalPacket.SequenceNumber };
                    FlatBufferBuilder b2 = new FlatBufferBuilder(1);
                    b2.Finish(ACK.Pack(b2, ack).Value);

                    if (internalPacket.ShouldRecvAck)
                    {
                        InternalPacketT p = new InternalPacketT
                        {
                            Type = "ACK",
                            ShouldRecvAck = false,
                            LastRequiredSeqNum = 0,
                            Payload = new List<byte>(b2.SizedByteArray())
                        };

                        b2 = new FlatBufferBuilder(1);
                        b2.Finish(InternalPacket.Pack(b2, p).Value);

                        m_Client.Send(b2.SizedByteArray(), b2.SizedByteArray().Length);
                    }

                    print("connected");
                    m_Client.Connect(IPAddress.Parse(host), int.Parse(port));

                    m_ToBeACK.Clear();
                    m_LastRecvPacket = 0;
                    m_LastACKpacket = 0;
                    m_LastRequiredSeqNum = 0;
                    m_NextSequenceNum = 1;

                    SYNT syn1 = new SYNT
                    {
                        LocalAddr = "",
                        RemoteAddr = syn.LocalAddr
                    };

                    b2 = new FlatBufferBuilder(1);
                    b2.Finish(SYN.Pack(b2, syn1).Value);

                    InternalPacketT internalPacket1 = new InternalPacketT
                    {
                        Type = "SYN",
                        ShouldRecvAck = true,
                        SequenceNumber = m_NextSequenceNum,
                        LastRequiredSeqNum = m_LastRequiredSeqNum,
                        Payload = new List<byte>(b2.SizedByteArray())
                    };

                    ms.SetLength(0);

                    if (internalPacket1.SequenceNumber > m_LastRequiredSeqNum && internalPacket1.ShouldRecvAck)
                        m_LastRequiredSeqNum = internalPacket1.SequenceNumber;
                    m_NextSequenceNum++;

                    b2 = new FlatBufferBuilder(1);
                    b2.Finish(InternalPacket.Pack(b2, internalPacket1).Value);


                    m_Client.Send(b2.SizedByteArray(), b2.SizedByteArray().Length);
                    ms.SetLength(0);


                    if (internalPacket1.ShouldRecvAck)
                    {
                        toBeAcknowledged toBeAcknowledged = new toBeAcknowledged
                        {
                            DeadLine = Time.time + Ping,
                            InternalPacket = internalPacket1
                        };
                        m_ToBeACK.Add(toBeAcknowledged.InternalPacket.SequenceNumber, toBeAcknowledged);
                    }

                    //PingAsync();
                    continue;
                }

                if (internalPacket.Type == "PING")
                {
                    m_Client.Send(data.Buffer, data.Buffer.Length);
                    continue;
                }

                if (internalPacket.Type == "PingClient")
                {
                    PingClient p = PingClient.GetRootAsPingClient(new ByteBuffer(internalPacket.Payload.ToArray()));
                    PingClientT pt = p.UnPack();
                    Ping = pt.Rtt;
                    ms.SetLength(0);

                    continue;
                }


                EvaluatePacket(internalPacket);
            }
        }

        m_Client.Close();
    }

    private void EvaluatePacket(InternalPacketT internalPacket)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            if (internalPacket.ShouldRecvAck)
            {
                ACKT ack = new ACKT { SequenceNumber = internalPacket.SequenceNumber };

                FlatBufferBuilder b = new FlatBufferBuilder(1);
                b.Finish(ACK.Pack(b, ack).Value);


                InternalPacketT p = new InternalPacketT
                {
                    Type = "ACK",
                    ShouldRecvAck = false,
                    LastRequiredSeqNum = 0,
                    Payload = new List<byte>(b.SizedByteArray())
                };

                b = new FlatBufferBuilder(1);
                b.Finish(InternalPacket.Pack(b, p).Value);

                m_Client.Send(b.SizedByteArray(), b.SizedByteArray().Length);
            }

            if (internalPacket.SequenceNumber == m_LastRecvPacket + 1)
            {
                RecvQ.Enqueue(internalPacket.Payload.ToArray());
                m_LastRecvPacket = internalPacket.SequenceNumber;

                if (internalPacket.ShouldRecvAck) m_LastACKpacket = internalPacket.SequenceNumber;
            }
            else if (internalPacket.SequenceNumber > m_LastRecvPacket + 1)
            {
                if (internalPacket.LastRequiredSeqNum == m_LastACKpacket)
                {
                    RecvQ.Enqueue(internalPacket.Payload.ToArray());
                    m_LastRecvPacket = internalPacket.SequenceNumber;
                    if (internalPacket.ShouldRecvAck) m_LastACKpacket = internalPacket.SequenceNumber;
                }
                else
                {
                    m_BackLog.Enqueue(internalPacket);
                }
            }
            else if (internalPacket.SequenceNumber < m_LastRecvPacket + 1)
            {
            }
        }
    }


    public async void CheckAckAsync()
    {
        using (MemoryStream ms = new MemoryStream())
        {
            while (!cts.Token.IsCancellationRequested)
            {
                List<ulong> temp = new List<ulong>();
                foreach (KeyValuePair<ulong, toBeAcknowledged> beAcknowledged in m_ToBeACK)
                    if (beAcknowledged.Value.DeadLine < Time.time)
                    {
                        FlatBufferBuilder b = new FlatBufferBuilder(1);
                        b.Finish(InternalPacket.Pack(b, beAcknowledged.Value.InternalPacket).Value);


                        m_Client.Send(b.SizedByteArray(), b.SizedByteArray().Length);

                        temp.Add(beAcknowledged.Key);
                    }

                foreach (ulong key in temp)
                {
                    toBeAcknowledged v = m_ToBeACK[key];
                    v.DeadLine = Time.time + Ping;
                    m_ToBeACK[key] = v;
                }

                await Task.Yield();
            }
        }
    }
}
//
// [Serializable]
// public struct InternalPacket
// {
//     public string Type;
//     public bool ShouldRecvACK;
//     public int SequenceNumber;
//     public int LastRequiredSeqNum;
//     public string Payload;
// }
//
// [Serializable]
// public struct ACK
// {
//     public int SequenceNumber;
// }
//
// [Serializable]
// public struct SYN
// {
//     public string LocalAddr;
//     public string RemoteAddr;
// }

[Serializable]
public struct toBeAcknowledged
{
    public float DeadLine;
    public InternalPacketT InternalPacket;
}
//
// [Serializable]
// public struct pingClient
// {
//     public int Ping;
// }